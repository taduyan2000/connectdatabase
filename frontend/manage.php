<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="manage.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <title>Manage</title>

</head>

<body>
    <?php include '/xampp/htdocs/taan/connectdatabase/backend/get_data.php';
    include '/xampp/htdocs/taan/connectdatabase/backend/regex_birtday.php';
    ?>

    <header>
        <div class="head">
            <div class="bar">
                <nav>
                    <ul class="nav">
                        <img src="https://cdn.iconscout.com/icon/free/png-256/user-1648810-1401302.png" width="150" style="float:left;">
                        <h1 style="margin-left: 10px; margin-top: 30px; font-size: 48px"><b>Quản Lý Người Dùng </b></h1>
                        <li><a href="manage.php" style="margin-top: 20px;"><b> Trang chủ</a></b></li>
                        <li><a href="Adduser.php" style="margin-top: 20px;"><b>Thêm thông tin user</b></a></li>
                        <li><a href="searchrs.php" style="margin-top: 20px;"><b>Tìm Thông tin user</b></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </header>

    <div class="output">
        <div id="container">
            <h2 style="text-align: center; position: absolute; margin-left: 600px"><b>Bảng Thông Tin User</b></h2>
            <div class="search">
                <form action="" method="post">
                    <input type="text" class="id" placeholder="Tìm kiếm theo Id">
                    <button type="submit" onclick="return alert('chức năng chưa phát triển')">Tìm</button>
                </form>
            </div>
            <table class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>First name</th>
                        <th>Mid name</th>
                        <th>Last name</th>
                        <th>Birthday</th>
                        <th>Address</th>
                        <th>Sex</th>
                        <th>Email</th>
                        <th>Xóa</th>
                        <th>Sửa</th>
                    </tr>
                </thead>

                <tbody>
                    <?php while ($row = $q->fetch()) : ?>
                        <tr>
                            <td><?php echo $id += 1; ?></td>
                            <td><?php echo ($row['ho']); ?></td>
                            <td><?php echo ($row['tendem']); ?></td>
                            <td><?php echo ($row['ten']); ?></td>
                            <td><?php echo preg_replace($pattern, "/", $row['ngay_sinh']); ?></td>
                            <td><?php echo ($row['dia_chi']); ?></td>
                            <td><?php echo ($row['gioi_tinh']); ?></td>
                            <td><?php echo ($row['email']); ?></td>
                            <td><button class="btn btn-warning"><a class="xoa" href="deleteuser.php?id=<?php echo ($row['id']) ?>" onClick="return confirm('Bạn có muốn xóa id này không ?')">Delete</a></button></td>
                            <td><button class="btn btn-danger" name="edit"><a href="edituser.php?id=<?php echo ($row['id']) ?>">Edit</a></button></td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>
            </table>
        </div>
    </div>

    <button class="btn btn-success" style="padding:15px 10px; margin-left: 20px; margin-bottom: 5px"><a href="adduser.php">Thêm user</a></button>

</body>

</html>