<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kết quả tìm kiếm</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="manage.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>
    <?php include '/xampp/htdocs/taan/connectdatabase/backend/get_data.php';
    include '/xampp/htdocs/taan/connectdatabase/backend/regex_birtday.php';
    ?>

    <div class="back">
        <nav>
            <ul class="nav">
                <li><a href="manage.php">Quay lại trang chủ</a></li>
                <li><a href="Adduser.php">Thêm thông tin user</a></li>
                <li><a href="deleteuser.php">Xóa user</a></li>
                <li><a href="edituser.php">Sửa thông tin user</a></li>
                <li><a href="searchrs.php">Tìm Thông tin user</a></li>
            </ul>
        </nav>
    </div>

    <div class="output">
        <div id="container">
            <h1 style="text-align: center;"><b>Kết quả tìm kiếm</b></h1>
            <table class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th>Id </th>
                        <th>First name</th>
                        <th>Mid name</th>
                        <th>Last name</th>
                        <th>Birthday</th>
                        <th>Address</th>
                        <th>Sex</th>
                    </tr>
                </thead>

                <tbody>
                    <?php while ($row = $query->fetch()) : ?>
                        <tr>
                            <td><?php echo ($row['id']) ?></td>
                            <td><?php echo ($row['ho']); ?></td>
                            <td><?php echo ($row['tendem']); ?></td>
                            <td><?php echo ($row['ten']); ?></td>
                            <td><?php echo preg_replace($pattern, "/", $row['ngay_sinh']); ?></td>
                            <td><?php echo ($row['dia_chi']); ?></td>
                            <td><?php echo ($row['gioi_tinh']); ?></td>
                        </tr>
                    <?php endwhile; ?>
                </tbody>

            </table>
        </div>

</body>

</html>