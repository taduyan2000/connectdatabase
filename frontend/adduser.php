<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="connect.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <title>Thêm user</title>

        <script>
            //phần script này dùng để kiểm tra xem có nhập đủ thông tin hay không (chưa hoàn thành )
            var get_fn = document.getElementById('first_name').value;
            var get_mn = document.getElementById('mid_name').value;
            var get_ln = document.getElementById('last_name').value;
            var get_bd = document.getElementById('birtday').value;
            var get_sm = document.getElementById('female').value;
            var get_sfm = document.getElementById('male').value;
            var get_ad = document.getElementById('address').value;
            var get_e = document.getElementById('email').value;

            // if (get_fn != "" && get_mn != "" && get_ln != "" && get_bd != "" && get_sm != "" && get_sfm != "" && get_ad != "" && get_e != '') {
            //     document.getElementById('first_name').style.borderColor = "green";
            //     document.getElementById('mid_name').style.borderColor = "green";
            //     document.getElementById('last_name').style.borderColor = "green";
            //     document.getElementById('birtday').style.borderColor = "green";
            //     document.getElementById('female').style.borderColor = "green";
            //     document.getElementById('male').style.borderColor = "green";
            //     document.getElementById('address').style.borderColor = "green";
            // } else {
            //     document.getElementById('first_name').style.borderColor = "red";
            //     document.getElementById('mid_name').style.borderColor = "red";
            //     document.getElementById('last_name').style.borderColor = "red";
            //     document.getElementById('birtday').style.borderColor = "red";
            //     document.getElementById('female').style.borderColor = "red";
            //     document.getElementById('male').style.borderColor = "red";
            //     document.getElementById('address').style.borderColor = "red";
            // }
            // window.onload = function() {
            //     document.getElementById(main_form).onsubmit = function() {return checkForm()}
            // }

            // function checkForm() {
            //     var valid = true;
            //     if (get_e.value == "") {
            //         document.getElementById('email').style.display = "block";
            //         // get_e.parentNode.className = "form-groip has-error has-feedback"
            //         valid = false;
            //     }
            //     return valid;
            // }
            // }
        </script>

    </head>

    <body>
        <?php include '/xampp/htdocs/taan/connectdatabase/backend/add.php'; ?>

        <div class="back">
            <nav>
                <ul class="nav">
                    <li><a href="manage.php">Quay lại trang chủ</a></li>
                    <li><a href="Adduser.php">Thêm thông tin user</a></li>
                    <li><a href="deleteuser.php">Xóa user</a></li>
                    <li><a href="edituser.php">Sửa thông tin user</a></li>
                </ul>
            </nav>
        </div>


        <div class="input">
            <h1 style="margin-left: 100px"><b>Thêm Thông Tin</b></h1>

            <form class="main_form" action="" method="post" style="text-align: left;">
                <label for="first_name">Họ</label><br>
                <input onchange="check()" type="text" id="first_name" name="first_name" value="<?php echo $first_name ?>"><br>

                <label for="mid_name">Tên đệm</label><br>
                <input type="text" id="mid_name" name="mid_name" value="<?php echo $mid_name; ?>"></input><br>

                <label for="last_name">Tên</label><br>
                <input type="text" id="last_name" name="last_name" value="<?php echo $last_name; ?>"><br>

                <label for="birtday">Ngày sinh</label><br>
                <input type="date" name="birtday" value="<?php echo $birtday; ?>"><br>
                <div>
                    <p style="float:left">
                        <label for="sex">Giới tính</label><br>
                    </p>
                    <p style="float:left; margin-left: 50px;">
                        <input type="radio" name="sex" value="Nam" id="male" checked="checked"><br>
                    <p style="float:left">
                        <label>Nam</label>
                    </p>
                    </p>
                    <p style="float:left; margin-left: 50px;">
                        <input type="radio" name="sex" value="Nữ" id="female"><br>
                    <p style="float:left">
                        <label>Nữ</label>
                    </p>
                    </p>
                </div><br>

                <label for="address" style="margin-right: 1000px; width: 100%;">Địa chỉ</label><br>
                <input type="text" name="address" value="<?php echo $address; ?>"><br>

                <label for="email">Email</label><br>
                <input type="text" name="email" value=""><br>

                <button type="submit">Thêm</button>
            </form>
        </div>

    </body>

    </html>