<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="connect.css">
	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	<title>Sửa Thông tin user</title>
</head>

<body>
	<?php
	include '/xampp/htdocs/taan/connectdatabase/backend/edit.php';
	include '/xampp/htdocs/taan/connectdatabase/backend/get_data.php';
	include '/xampp/htdocs/taan/connectdatabase/backend/regex_birtday.php';
                 
	if (isset($_GET["id"])) {
		$id = $_GET['id'];
	} else {
		header('location:./manage.php');
	}

	$sql4 = "SELECT*FROM user_info WHERE id = '$id'";
	$query = $db->prepare($sql4);
	$query->execute(array(':id' => $id));

	while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
		$id = $row['id'];
		$first_name = $row['ho'];
		$mid_name = $row['tendem'];
		$last_name = $row['ten'];
		$birtday = $row['ngay_sinh'];
		$sex = $row['gioi_tinh'];
		$address = $row['dia_chi'];
		$email = $row['email'];
	}
	$idst ='';
	if (isset($_POST["idst"])) {
		$idst = $_POST['idst'];
	}

	?>

	<div class="back">
		<nav>
			<ul class="nav">
				<li><a href="manage.php">Quay lại trang chủ</a></li>
				<li><a href="Adduser.php">Thêm thông tin user</a></li>
				<li><a href="deleteuser.php">Xóa user</a></li>
				<li><a href="edituser.php">Sửa thông tin user</a></li>
			</ul>
		</nav>
	</div>

	<div class="input">
		<h1 style="margin-left: 100px; position: absolute;"><b>Sửa Thông tin</b></h1>

		<form action="" method="post" style="text-align: left;">
			<input type="hidden" id="idst" name="idst" value=""><br>
			<input type="hidden" id="id" name="id" value="<?php echo $_GET['id']; ?>"><br>

			<label for="first_name">Họ</label><br>
			<input type="text" id="first_name" name="first_name" value="<?php echo $first_name ?>"><br>

			<label for="mid_name">Tên đệm</label><br>
			<input type="text" id="mid_name" name="mid_name" value="<?php echo $mid_name; ?>"></input><br>

			<label for="last_name">Tên</label><br>
			<input type="text" id="last_name" name="last_name" value="<?php echo $last_name; ?>"><br>

			<label for="birtday">Ngày sinh</label><br>
			<input type="date" name="birtday" value="<?php echo $birtday; ?>"><br>

			<div>
				<p style="float:left">
					<label for="sex">Giới tính</label><br>
				</p>
				<p style="float:left; margin-left: 50px;">
					<input type="radio" name="sex" value="Nam" id="male" checked="checked"><br>
				<p style="float:left">
					<label>Nam</label>
				</p>
				</p>
				<p style="float:left; margin-left: 50px;">
					<input type="radio" name="sex" value="Nữ" id="female"><br>
				<p style="float:left">
					<label>Nữ</label>
				</p>
				</p>
			</div><br>

			<label for="address" style="margin-right: 1000px; width: 100%;">Địa chỉ</label><br>
			<input type="text" name="address" value="<?php echo $address; ?>"><br>

			<label for="email">Email</label><br>
			<input type="text" name="email" value="<?php echo $email; ?>"><br>

			<button type="submit">Sửa</button>
		</form>

	</div>

	<div class="output">
		<div id="container">
			<h2 style="text-align: center;"><b>Bảng Thông Tin User</b></h2>
			<table class="table table-bordered table-condensed table-striped">
				<thead>
					<tr>
						<th>STT</th>
						<th>First name</th>
						<th>Mid name</th>
						<th>Last name</th>
						<th>Birthday</th>
						<th>Address</th>
						<th>Sex</th>
						<th>Email</th>
						<th>Xóa</th>
						<th>Sửa</th>
					</tr>
				</thead>

				<tbody>
					<?php while ($row = $q->fetch()) : ?>
						<tr>
							<td><?php echo $idst += 1; ?></td>
							<td><?php echo ($row['ho']); ?></td>
							<td><?php echo ($row['tendem']); ?></td>
							<td><?php echo ($row['ten']); ?></td>
							<td><?php echo preg_replace($pattern, "/", $row['ngay_sinh']); ?></td>
							<td><?php echo ($row['dia_chi']); ?></td>
							<td><?php echo ($row['gioi_tinh']); ?></td>
							<td><?php echo ($row['email']); ?></td>
							<td><button class="btn btn-warning"><a class="xoa" href="deleteuser.php?id=<?php echo ($row['id']) ?>" onClick="return confirm('Bạn có muốn xóa id này không ?')">Delete</a></button></td>
							<td><button class="btn btn-danger" name="edit"><a href="edituser.php?id=<?php echo ($row['id']) ?>">Edit</a></button></td>
						</tr>
					<?php endwhile; ?>
				</tbody>
			</table>
		</div>
	</div>

</body>

</html>